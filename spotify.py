import os
import requests
import urllib.parse
from flask import Flask, request, render_template
import random
import threading
import logging
import sys


class SpotifyAPI(object):
    # TODO Search album by name/artist
    # TODO Search track by name/artist

    def __init__(self):
        super().__init__()

        # Rather ugly way to get the id token. Must be a better way
        self.app = Flask(__name__)

        self.token = ""
        self.token_type = ""
        self.user_id = ""

        # Disable flask output
        log = logging.getLogger('werkzeug')
        log.setLevel(logging.ERROR)
        cli = sys.modules['flask.cli']
        cli.show_server_banner = lambda *x: None

        @self.app.route('/')
        def index():
            params = {
                    "client_id": os.getenv("SPOTIFY_CLIENT_ID"),
                    "response_type": "token",
                    "redirect_uri": f"http://{os.getenv('HOST')}:{os.getenv('PORT')}/callback",
                    "scope": "playlist-modify-public"
                    }
            url = "https://accounts.spotify.com/authorize?" + urllib.parse.urlencode(params)
            return render_template("login.html", redirect_url=url)

        @self.app.route('/callback')
        def callback():
            return '''  <script type="text/javascript">
                        var token = window.location.href.split("access_token=")[1];
                        window.location = "/token/" + token;
                    </script> '''

        @self.app.route('/token/<token_string>/')
        def token(token_string):
            self.token, self.token_type, _ = token_string.split('&')
            self.token_type = self.token_type.replace('token_type=', '')
            self.shutdown_server()
            return "You are logged in. You can close this window."

    def shutdown_server(self):
        func = request.environ.get('werkzeug.server.shutdown')
        if func is None:
            raise RuntimeError('Not running with the Werkzeug Server')
        func()

    # Util
    def uriFromUrl(self, url):
        return url.split("?")[0].replace("https://open.spotify.com/", "spotify:").replace("/", ":")

    # Util
    def parseTrack(self, track):
        if track.startswith("https://open.spotify.com/track"):  # Spotify track url
            return self.uriFromUrl(track)
        elif track.startswith("spotify:track"):  # Spotify track uri
            return track
        elif track.startswith("https://open.spotify.com/album"):  # Spotify track url
            track_uri = self.uriFromUrl(track)
            return self.getRandomTrackFromAlbum(track_uri)
        elif track.startswith("spotify:album"):  # Spotify track uri
            return self.getRandomTrackFromAlbum(track)
        else:
            print("Lien inconnu. Il faudrait rajouter un log d'erreur")

    # Util
    def parseTracks(self, tracks):
        return list(map(self.parseTrack, tracks))

    def login(self):
        t = threading.Thread(target=self.app.run, kwargs={'host': os.getenv('HOST'), 'port': os.getenv('PORT')})
        t.start()

        # time.sleep(2)
        # params = {
        #         "client_id": os.getenv("SPOTIFY_CLIENT_ID"),
        #         "response_type": "token",
        #         "redirect_uri": f"http://{os.getenv('HOST')}:{os.getenv('PORT')}/",
        #         "scope": "playlist-modify-public"
        #         }
        # url = "https://accounts.spotify.com/authorize?" + urllib.parse.urlencode(params)
        # webbrowser.open(url)
        t.join()

    def _getUserId(self):
        headers = {"Authorization": f"{self.token_type} {self.token}"}
        r = requests.get("https://api.spotify.com/v1/me", headers=headers)
        self.user_id = r.json()['id']

    def createPlaylist(self, name):
        if self.user_id == "":
            self._getUserId()
        data = {"name": name}
        headers = {"Authorization": f"{self.token_type} {self.token}", "Content-Type": "application/json"}
        r = requests.post(f"https://api.spotify.com/v1/users/{self.user_id}/playlists", json=data, headers=headers)
        rjson = r.json()
        spotify_id = rjson['id']
        return rjson['external_urls']['spotify'], spotify_id, rjson['snapshot_id']

    def deletePlaylist(self, playlist_id):
        if self.user_id == "":
            self._getUserId()
        headers = {"Authorization": f"{self.token_type} {self.token}"}
        r = requests.delete(f"https://api.spotify.com/v1/playlists/{playlist_id}/followers", headers=headers)
        assert r.status_code == 200

    def addTracks(self, playlist_id, tracks):
        if self.user_id == "":
            self._getUserId()
        headers = {"Authorization": f"{self.token_type} {self.token}", "Content-Type": "application/json"}
        data = {"uris": self.parseTracks(tracks)}
        try:
            r = requests.post(f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks", json=data, headers=headers)
        except KeyError:
            raise SpotifyAPI.PlaylistError("Playlist not found", "Use createPlaylist to create a playlist")
        return r.json()['snapshot_id']

    def removeTracks(self, playlist_id, tracks):
        if self.user_id == "":
            self._getUserId()
        headers = {"Authorization": f"{self.token_type} {self.token}", "Content-Type": "application/json"}
        data = {"tracks": [{"uri": value} for value in self.parseTracks(tracks)]}
        try:
            r = requests.delete(f"https://api.spotify.com/v1/playlists/{playlist_id}/tracks", json=data, headers=headers)
        except KeyError:
            raise SpotifyAPI.PlaylistError("Playlist not found", "Use createPlaylist to create a playlist")
        return r.json()['snapshot_id']

    def getRandomTrackFromAlbum(self, album):
        if self.user_id == "":
            self._getUserId()

        if album.startswith("https://open.spotify.com/album"):  # Spotify album url
            album_uri = self.uriFromUrl(album)
        elif album.startswith("spotify:album"):  # Spotify album uri
            album_uri = album
        else:
            raise SpotifyAPI.BadIdentifier("Could not find album with this identifier", "Check if you have a valid spotify URI or URL")
        album_id = album_uri.split(":")[-1]

        headers = {"Authorization": f"{self.token_type} {self.token}", "Content-Type": "application/json"}
        r = requests.get(f"https://api.spotify.com/v1/albums/{album_id}", headers=headers)

        rand_index = random.randrange(len(r.json()['tracks']['items']))
        return r.json()['tracks']['items'][rand_index]['uri']

    class PlaylistError(Exception):
        """Exception raised for inexistant playlists.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
        """

        def __init__(self, expression, message):
            self.expression = expression
            self.message = message

    class BadIdentifier(Exception):
        """Exception raised for inexistant playlists.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
        """

        def __init__(self, expression, message):
            self.expression = expression
            self.message = message
