import os
import random
import string
import sqlite3
from datetime import datetime

class PacomeDB(object):
    def __init__(self, *args):
        super().__init__()
        self.ID_SIZE = int(os.getenv("ID_SIZE"))
        if len(args):
            assert isinstance(args[0], str)
            self.DB_NAME = args[0]
        else:
            self.DB_NAME = os.getenv("DB_NAME")

        # Init database
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f"CREATE TABLE IF NOT EXISTS users (\
                id integer primary key autoincrement,\
                user_tgId text,\
                user_active_group id,\
                user_lang integer default 0)")
        c.execute(f"CREATE TABLE IF NOT EXISTS groups (\
                id integer primary key autoincrement,\
                group_id text, group_name text,\
                group_playlist_url text,\
                group_playlist_id text,\
                group_admin integer,\
                group_threshold integer default 0)")
        c.execute(f"CREATE TABLE IF NOT EXISTS userGroupRel (\
                id integer primary key autoincrement,\
                group_id integer,\
                user_id integer)")
        c.execute(f"CREATE TABLE IF NOT EXISTS chanGroupBinding (\
                id integer primary key autoincrement,\
                group_id integer,\
                chan_tgId integer)")
        c.execute(f"CREATE TABLE IF NOT EXISTS trackStack (\
                id integer primary key autoincrement,\
                group_id integer,\
                user_id integer,\
                track_url text,\
                votes_for integer default 0,\
                votes_against integer default 0,\
                action integer,\
                date text,\
                dead integer default 0)")  # action : 0 for removing 1 for adding
        c.execute(f"CREATE TABLE IF NOT EXISTS votes (\
                id integer primary key autoincrement,\
                user_id integer,\
                track_id text,\
                vote integer,\
                date text)")
        conn.commit()
        conn.close()

    # Fancy
    def getGroupId(self):

        group_id = ''.join(random.SystemRandom().choice(string.ascii_uppercase
                                                        + string.ascii_lowercase
                                                        + string.digits) for _ in range(self.ID_SIZE))
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'SELECT COUNT(*) FROM groups WHERE group_id="{group_id}"')
        count = c.fetchone()[0]
        conn.close()
        if count:
            group_id = self.getGoupId()
        return group_id

    # Fancy
    def getAllUsers(self):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute('SELECT * FROM users')
        users = c.fetchall()
        conn.close()
        return users

    # Generic
    def getUser(self, *fields, **conditions):
        sql = 'SELECT {} FROM users'
        if fields:
            sql = sql.format(', '.join(fields))
        else:
            sql.format('*')
        if conditions:
            clause = ' WHERE {}'.format(', '.join('{}=?'.format(k) for k in conditions))
            sql = sql + clause
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(sql, tuple(conditions.values()))
        result = c.fetchall()
        conn.close()
        return result

    # Generic
    def getGroup(self, *fields, **conditions):
        sql = 'SELECT {} FROM groups'
        if fields:
            sql = sql.format(', '.join(fields))
        else:
            sql.format('*')
        if conditions:
            clause = ' WHERE {}'.format(', '.join('{}=?'.format(k) for k in conditions))
            sql = sql + clause
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(sql, tuple(conditions.values()))
        result = c.fetchall()
        conn.close()
        return result

    # Generic
    def getTrack(self, *fields, **conditions):
        sql = 'SELECT {} FROM trackStack'
        if fields:
            sql = sql.format(', '.join(fields))
        else:
            sql = sql.format('*')
        if conditions:
            clause = ' WHERE {}'.format(', '.join('{}=?'.format(k) for k in conditions))
            sql = sql + clause
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(sql, tuple(conditions.values()))
        result = c.fetchall()
        conn.close()
        return result

    # Generic
    def updateTrack(self, track_id, **kwargs):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        sql = 'UPDATE trackStack SET {} WHERE id={}'.format(', '.join('{}=?'.format(k) for k in kwargs), track_id)
        c.execute(sql, tuple(kwargs.values()))
        conn.commit()
        conn.close()

    # Fancy
    def createTrack(self, group_id, user_id, track_url, action):
        sql = f"INSERT INTO trackStack (group_id, user_id, track_url, action, date) VALUES (?, ?, ?, ?, ?)"
        t = (group_id, user_id, track_url, action, datetime.utcnow().isoformat())
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(sql, t)
        conn.commit()
        track_id = c.lastrowid
        conn.close()
        return track_id

    # Fancy
    def createVote(self, user_id, track_id, vote):
        sql = f"INSERT INTO votes (user_id, track_id, vote, date) VALUES (?, ?, ?, ?, ?)"
        t = (user_id, track_id, vote, datetime.utcnow().isoformat())
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(sql, t)
        conn.commit()
        track_id = c.lastrowid
        conn.close()
        return track_id

    # Fancy
    def hasVoted(self, user_id, track_id):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'SELECT COUNT(*) FROM votes WHERE user_id={user_id} AND track_id={track_id}')
        count = c.fetchone()[0]
        conn.close()
        return bool(count)

    # Fancy
    def isMember(self, user_id, group_id):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'SELECT COUNT(*) FROM userGroupRel WHERE user_id={user_id} AND group_id={group_id}')
        count = c.fetchone()[0]
        conn.close()
        return bool(count)

    # Fancy
    def getUserGroups(self, user_id):
        sql = f'SELECT id, group_name, group_id FROM groups WHERE id in (SELECT group_id FROM userGroupRel WHERE user_id={user_id})'
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(sql)
        result = c.fetchall()
        conn.close()
        return result

    # Fancy
    def getUserId(self, user_tgid):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'SELECT id FROM users WHERE user_tgId="{user_tgid}"')
        users = c.fetchone()
        if users:
            user_id = users[0]
            conn.close()
            return user_id
        else:
            raise PacomeDB.UnknownUserError("Unknown user", "Check if it is present in the database")

    # Fancy
    def getUserLang(self, user_tgid):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'SELECT user_lang FROM users WHERE user_tgId="{user_tgid}"')
        users = c.fetchone()
        if users:
            user_lang = users[0]
            conn.close()
            return user_lang
        else:
            raise PacomeDB.UnknownUserError("Unknown user", "Check if it is present in the database")

    # Generic
    def updateUser(self, user_id, **kwargs):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        sql = 'UPDATE users SET {} WHERE id={}'.format(', '.join('{}=?'.format(k) for k in kwargs), user_id)
        c.execute(sql, tuple(kwargs.values()))
        conn.commit()
        conn.close()

    # Fancy
    def getUserActivePlaylist(self, user_tgid):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'SELECT group_playlist_id FROM groups WHERE id=(SELECT user_active_group FROM users WHERE user_tgId="{user_tgid}")')
        playlist = c.fetchone()
        conn.close()
        return playlist[0]

    # Generic
    def createGroup(self, name, user_tgid, playlist_url, playlist_id, group_threshold):
        group_id = self.getGroupId()
        user_id = self.getUserId(user_tgid)

        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'INSERT INTO groups (group_id, group_name, group_playlist_url, group_playlist_id, group_admin, group_threshold) VALUES ("{group_id}", "{name}", "{playlist_url}", "{playlist_id}", {user_id}, {group_threshold})')
        conn.commit()
        conn.close()
        return name, group_id, playlist_url

    # Generic
    def registerUser(self, user_tgid):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'SELECT id FROM users WHERE user_tgId="{user_tgid}"')
        users = c.fetchone()
        if users is None:
            c.execute(f'INSERT INTO users (user_tgId) VALUES ("{user_tgid}")')
            user_id = c.lastrowid
            conn.commit()
            conn.close()
        else:
            user_id = users[0]
            conn.close()
        return user_id

    # Fancy
    def createBinding(self, chan_tgid, group_id):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'INSERT INTO chanGroupBinding (group_id, chan_tgId) VALUES ({group_id}, "{chan_tgid}")')
        conn.commit()
        conn.close()

    # Fancy
    def getChanBinding(self, chan_tgid):
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'SELECT group_id FROM chanGroupBinding WHERE chan_tgId="{chan_tgid}"')
        group_id = c.fetchone()[0]
        conn.close()
        return group_id

    # Fancy
    def join(self, group_id, user_tgid):
        user_id = self.getUserId(user_tgid)
        conn = sqlite3.connect(f"{self.DB_NAME}.db")
        c = conn.cursor()
        c.execute(f'SELECT id, group_name, group_playlist_url  FROM groups WHERE group_id="{group_id}"')
        group = c.fetchone()
        if group:
            c.execute(f'INSERT INTO userGroupRel (group_id, user_id) VALUES ({group[0]}, {user_id})')
            c.execute(f'UPDATE users SET user_active_group="{group[0]}" WHERE user_tgId="{user_tgid}"')
            conn.commit()
            conn.close()
            return group[1:]
        else:
            conn.close()
            raise self.UnknownGroupError("Unknown Group", "Make sure is has been created")

    def delete(self):
        os.remove(f"{self.DB_NAME}.db")

    class UnknownUserError(Exception):
        """Exception raised for inexistant playlists.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
        """

        def __init__(self, expression, message):
            self.expression = expression
            self.message = message

    class UnknownGroupError(Exception):
        """Exception raised for inexistant playlists.

        Attributes:
            expression -- input expression in which the error occurred
            message -- explanation of the error
        """

        def __init__(self, expression, message):
            self.expression = expression
            self.message = message
