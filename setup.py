import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="pacome", # Replace with your own username
    version="0.0.1",
    author="donatiend",
    author_email="donatien@odena.eu",
    description="A telegram bot for making spotify playlists",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://gitlab.com/donatiend/pacome",
    packages=setuptools.find_packages(),
    install_requires=[
        'certifi==2020.4.5.1',
        'chardet==3.0.4',
        'click==7.1.1',
        'entrypoints==0.3',
        'Flask==1.1.2',
        'idna==2.9'
        'itsdangerous==1.1.0',
        'Jinja2==2.11.1',
        'MarkupSafe==1.1.1',
        'mccabe==0.6.1',
        'pyTelegramBotAPI==3.6.7',
        'python-dotenv==0.12.0',
        'requests==2.23.0',
        'six==1.14.0',
        'urllib3==1.25.8',
        'Werkzeug==1.0.1',
    ],
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: GNU GPLv3",
        "Operating System :: OS Independent",
    ],
    python_requires='>=3.8',
)
