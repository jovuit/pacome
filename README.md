# Introduction

In this repository lies the code of the telegram bot named Pacôme whose purpose in life is to help groups of people manage spotify playlists collaboratively with a voting system.

# How can I use this bot

Find @PacomeBot in the telegram search bar and send him the start command to make sure he knows you.
Once this is done you have two possible choices :


## Create your own group 

- /creategroup `GROUP_NAME`  : This will create a playlist and return you your playlist url and the code to share for joining the group
- Share your code
- (Optional) If you to source your tracks from telegram group conversations see **Bind the bot to a group conversation**


### Bind the bot to a group conversation

As specified in **Manage tracks** you have two possibilities for adding tracks to the group's playlist : sourcing from private messages or sourcing from group conversation. If you want your members to be able to add tracks directly from your group conversation you need to follow this steps :

- Add your bot to the group conversation as an admin.
- Send /bind `GROUP_CODE` to the group conversation

Once this is done all the members of the group can add tracks simply by mentioning the bot's name in the message containing the track's url. If someone who is not a member of the pacome group bound with the telegram conversation tries to add a track he will receive a private message stating that the bot doesn't know them.


## Join an existing group

To do this, you need the code of a group to join. It is a 6 character long string containing lower and uppercase letters and numbers.

- /join `GROUP_CODE` : You are now a member of the group. This command will return you the url of the group's playlist and you will receive a notification whenever a new track is added.

## Manage tracks

To add track you can either send /addtrack `TRACK_URL` to the bot by private message or add @PacomeBot in a message of a group conversation.

To remove track you need to send /removetrack `TRACK_URL` by private message.

Both messages will trigger a vote.

## Restrictions

So far the bot can only work with spotify urls or uris. If you send a link to an album, the bot will process the input by randomly choosing a track in the album. If you send a deezer or youtube or whatever link, il will be ignored by the bot.


# How can I run my own instance of the bot


First you need to have a spotify account a to declare a bot name with @BotFather. This will give you a token that you need to keep.

Then copy this repository and create a .env file from .env.template. This is where you need to specify your telegram token amongst other things.
Now you need to understand that in order to allow the bot to manage playlists he has to be authorized to access the spotify api. This is done by sending requests from a web server. This web server will need a HOST and a PORT, once you have it (you can use localhost if you want tu run your bot from your computer) you need to go in te spotify dev dashboard, create an app and add http://HOST:PORT/callback to its redirect URL.

If everything has been done correctly, you sould be able to run `pip install - r requirements.txt` and the `python start.py` to run your bot. Once the program launched, send /start to your bot and then /login. Go to http://HOST:PORT on your favorite web brower, click the login link and log in to Spotify.

You are now all set and can use you bot. Have fun !
