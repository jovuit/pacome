import os

from dotenv import load_dotenv

import telebot

from playlistManager import PlaylistManager
from database import PacomeDB

import strings
import logging
import time

# TODO Message when there should be input but none is given

load_dotenv()

GROUP_CODE = os.getenv("GROUP_CODE")
SECRET_ADMN_CODE = os.getenv("SECRET_ADMIN_CODE")
TELEGRAM_TOKEN = os.getenv("TELEGRAM_TOKEN")

PLAYLIST = os.getenv("PLAYLIST")

lang = 0

logger = telebot.logger
telebot.logger.setLevel(logging.INFO)

pacome = telebot.TeleBot(TELEGRAM_TOKEN)
db = PacomeDB()
logger.info("DB started")
pm = PlaylistManager(db)
logger.info("PlaylistManager started")

actions = ('removeTrack', 'addTrack')


def triggerVote(group_id, group_name, track, track_id, action):
    if type(track_id) == list :
        for i, id in enumerate(track_id):
            triggerVote(group_id, group_name, track[i], id, action)
    else:
        users = db.getUser('id', 'user_tgId', 'user_lang', user_active_group=group_id)
        for user_id, user, lang in users:
            markup = telebot.types.InlineKeyboardMarkup()
            itembtn1 = telebot.types.InlineKeyboardButton(u"\U0001F44D", callback_data=f"vote1:{track_id}")
            itembtn2 = telebot.types.InlineKeyboardButton(u"\U0001F44E", callback_data=f"vote0:{track_id}")
            markup.add(itembtn1, itembtn2)
            pacome.send_message(user, strings.strings['vote'][lang].format(group_name, actions[action], track), parse_mode='Markdown', reply_markup=markup)


def handle_messages(messages):
    for message in messages:
        if message.chat.type == "group" or message.chat.type == "supergroup":
            valid = False
            url = None
            if message.entities:
                for entity in message.entities:
                    if entity.type == 'mention':
                        valid = (message.text[entity.offset:entity.offset + entity.length] == '@PacomeBot')
                    if entity.type == 'url':
                        url = message.text[entity.offset:entity.offset + entity.length]
                if valid and url:
                    message.text = url
                    group_id = db.getChanBinding(message.chat.id)
                    addTrack(message, group_id)


@pacome.message_handler(commands=['help'])
def help(message):
    logger.info("Received help command")
    try:
        pacome.send_message(message.chat.id, strings.strings['help'][db.getUserLang(message.from_user.id)])
    except PacomeDB.UnknownUserError:
        pacome.send_message(message.chat.id, strings.strings['help'][0])


@pacome.message_handler(commands=['bind'])
def bind(message):
    logger.info("Received bind command")
    msg = message.text.replace('/bind', '').strip()
    if len(msg):
        try:
            group_id = db.getGroup('id', group_id=msg)[0][0]
            print(group_id)
            user_id = db.getUserId(message.from_user.id)
            print(user_id)
            if db.isMember(user_id, group_id):
                db.createBinding(message.chat.id, group_id)
        except PacomeDB.UnknownUserError:
            pacome.send_message(message.chat.id, strings.strings['help'][0])


@pacome.message_handler(commands=['login'])
def login(message):
    logger.info("Received login command")
    pm.login()


@pacome.message_handler(commands=['start'])
def start(message):
    logger.info("Received start command")
    _ = db.registerUser(message.from_user.id)
    pacome.send_message(message.chat.id, strings.strings['hello'][lang], parse_mode='Markdown')


@pacome.message_handler(commands=['join'])
def join(message):
    logger.info("Received join command")
    msg = message.text.replace('/join', '').strip()
    if len(msg):
        try:
            group, playlist = db.join(msg, message.from_user.id)
            pacome.send_message(message.chat.id, strings.strings['joined'][db.getUserLang(message.from_user.id)].format(group, playlist), parse_mode='Markdown')
        except PacomeDB.UnknownGroupError:
            print("TODO")
        except PacomeDB.UnknownUserError:
            pacome.send_message(message.chat.id, strings.strings['unknown_user'][lang])
    else:
        print("TODO")


@pacome.message_handler(commands=['addtrack'])
def addTrack(message, group_id=None):
    logger.info("Received addTrack command")
    msg = message.text.replace('/addtrack', '').strip()
    # if admin addTrackAdmin
    if len(msg):
        try:
            user_id = db.getUserId(message.from_user.id)
            if not group_id:
                group_id = db.getUser('user_active_group', id=user_id)[0][0]
            active_group_name = db.getGroup('group_name', id=group_id)[0][0]
            track_id, group_id = pm.addTrack(msg, group_id, user_id)
            pacome.send_message(message.from_user.id, strings.strings['add_track'][db.getUserLang(message.from_user.id)].format(active_group_name), parse_mode='Markdown')
            triggerVote(group_id, active_group_name, msg, track_id, 1)
        except PacomeDB.UnknownUserError:
            pacome.send_message(message.from_user.id, strings.strings['unknown_user'][lang])


@pacome.message_handler(commands=['addtracks'])
def addTracks(message):
    logger.info("Received addTracks command")
    msg = message.text.replace('/addtracks', '').strip()
    # if admin addTracksAdmin
    if len(msg):
        try:
            user_id = db.getUserId(message.from_user.id)
            active_group = db.getUser('user_active_group', id=user_id)[0][0]
            active_group_name = db.getGroup('group_name', id=active_group)[0][0]
            tracks_id, group_id = pm.addTracks(msg.split(','), active_group, user_id)
            pacome.send_message(message.chat.id, strings.strings['add_tracks'][db.getUserLang(message.from_user.id)].format(active_group_name), parse_mode='Markdown')
            triggerVote(group_id, active_group_name, msg.split(','), tracks_id, 1)
        except PacomeDB.UnknownUserError:
            pacome.send_message(message.chat.id, strings.strings['unknown_user'][lang])


@pacome.message_handler(commands=['removetrack'])
def removeTrack(message):
    logger.info("Received removeTrack command")
    msg = message.text.replace('/removetrack', '').strip()
    # if admin removeTrackAdmin
    if len(msg):
        try:
            user_id = db.getUserId(message.from_user.id)
            active_group = db.getUser('user_active_group', id=user_id)[0][0]
            active_group_name = db.getGroup('group_name', id=active_group)[0][0]
            track_id, group_id = pm.removeTrack(msg, active_group, user_id)
            pacome.send_message(message.chat.id, strings.strings['remove_track'][db.getUserLang(message.from_user.id)].format(active_group_name), parse_mode='Markdown')
            triggerVote(group_id, active_group_name, msg, track_id, 0)
        except PacomeDB.UnknownUserError:
            pacome.send_message(message.chat.id, strings.strings['unknown_user'][lang])


@pacome.message_handler(commands=['removetracks'])
def removeTracks(message):
    logger.info("Received removeTracks command")
    msg = message.text.replace('/removetracks', '').strip()
    # if admin removeTrackAdmin
    if len(msg):
        try:
            user_id = db.getUserId(message.from_user.id)
            active_group = db.getUser('user_active_group', id=user_id)[0][0]
            active_group_name = db.getGroup('group_name', id=active_group)[0][0]
            tracks_id, group_id = pm.removeTracks(msg.split(','), active_group, user_id)
            pacome.send_message(message.chat.id, strings.strings['remove_tracks'][db.getUserLang(message.from_user.id)].format(active_group_name), parse_mode='Markdown')
            triggerVote(group_id, active_group_name, msg.split(','), tracks_id, 0)
        except PacomeDB.UnknownUserError:
            pacome.send_message(message.chat.id, strings.strings['unknown_user'][lang])


@pacome.message_handler(commands=['creategroup'])
def createGroup(message):
    logger.info("Received createGroup command")
    msg = message.text.replace('/creategroup', '').strip()
    if len(msg):
        try:
            playlist_url, playlist_id, _ = pm.createPlaylist(msg)
            # Make group threshold a parameter
            group_name, group_id, playlist_url = db.createGroup(msg, message.from_user.id, playlist_url, playlist_id, 2)
            _, _ = db.join(group_id, message.from_user.id)
            pacome.send_message(message.chat.id, strings.strings['group_created'][db.getUserLang(message.from_user.id)].format(group_name, group_id, playlist_url), parse_mode='Markdown')
        except PacomeDB.UnknownUserError:
            pacome.send_message(message.chat.id, strings.strings['unknown_user'][lang])


@pacome.message_handler(commands=['mygroups'])
def myGroups(message):
    logger.info("Received myGroups command")
    user_id = db.getUserId(message.from_user.id)
    active_group = db.getUser('user_active_group', id=user_id)[0][0]
    all_groups = db.getUserGroups(user_id)
    groups = ['●  *{}* `{}`'.format(g[1], g[2]) if g[0] == active_group else '○  *{}* `{}`'.format(g[1], g[2]) for g in all_groups]
    try:
        pacome.send_message(message.from_user.id, strings.strings['my_groups'][db.getUserLang(message.from_user.id)].format('\n'.join(groups)), parse_mode='Markdown')
    except PacomeDB.UnknownUserError:
        pacome.send_message(message.chat.id, strings.strings['unknown_user'][lang])


@pacome.message_handler(commands=['group'])
def changeGroup(message):
    logger.info("Received group command")
    user_id = db.getUserId(message.from_user.id)
    all_groups = db.getUserGroups(user_id)
    markup = telebot.types.InlineKeyboardMarkup()
    for g in all_groups:
        itembtn = telebot.types.InlineKeyboardButton(str(g[1]), callback_data=f"group{g[0]}")
        markup.add(itembtn)
    try:
        pacome.send_message(message.from_user.id, strings.strings['change_group'][db.getUserLang(message.from_user.id)], reply_markup=markup)
    except PacomeDB.UnknownUserError:
        pacome.send_message(message.chat.id, strings.strings['unknown_user'][lang])


@pacome.message_handler(commands=['lang'])
def changeLang(message):
    logger.info("Received lang command")
    markup = telebot.types.InlineKeyboardMarkup()
    itembtn1 = telebot.types.InlineKeyboardButton(u"\U0001F1EC\U0001F1E7", callback_data="lang0")
    itembtn2 = telebot.types.InlineKeyboardButton(u"\U0001F1EB\U0001F1F7", callback_data="lang1")
    markup.add(itembtn1, itembtn2)
    pacome.send_message(message.from_user.id, "Choose your language:", reply_markup=markup)


@pacome.callback_query_handler(func=lambda call: call.data)
def callback(call):
    if call.data.startswith('lang'):
        user_id = db.getUserId(call.from_user.id)
        db.updateUser(user_id, user_lang=int(call.data.replace('lang', '')))
        pacome.answer_callback_query(call.id)
        try:
            pacome.send_message(call.from_user.id, strings.strings['lang_changed'][db.getUserLang(call.from_user.id)])
        except PacomeDB.UnknownUserError:
            pacome.send_message(call.from_user.id, strings.strings['unknown_user'][lang])
    elif call.data.startswith('group'):
        user_id = db.getUserId(call.from_user.id)
        db.updateUser(user_id, user_active_group=int(call.data.replace('group', '')))
        pacome.answer_callback_query(call.id)
        try:
            pacome.send_message(call.from_user.id, strings.strings['group_changed'][db.getUserLang(call.from_user.id)])
        except PacomeDB.UnknownUserError:
            pacome.send_message(call.from_user.id, strings.strings['unknown_user'][lang])
    elif call.data.startswith('vote'):
        #check people only vote once
        vote, track_id = call.data.replace('vote', '').split(':')
        user_id = db.getUserId(call.from_user.id)
        if not db.hasVoted(user_id, track_id):
            pm.vote(track_id, int(vote))
            pacome.answer_callback_query(call.id)
            try:
                pacome.send_message(call.from_user.id, strings.strings['voted'][db.getUserLang(call.from_user.id)])
            except PacomeDB.UnknownUserError:
                pacome.send_message(call.from_user.id, strings.strings['unknown_user'][lang])


pacome.set_update_listener(handle_messages)
pacome.polling(none_stop=True)
# while True:
# 
#     try:
# 
#         pacome.polling(none_stop=True)
# 
#     # ConnectionError and ReadTimeout because of possible timout of the requests library
# 
#     # TypeError for moviepy errors
# 
#     # maybe there are others, therefore Exception
# 
#     except KeyError as e:
# 
#         logger.error(e)
# 
#         time.sleep(15)
