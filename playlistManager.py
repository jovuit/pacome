from spotify import SpotifyAPI
from database import PacomeDB
from datetime import datetime, timedelta

# spotifyAPI.login()

class PlaylistManager(object):
    def __init__(self, db):
        super().__init__()
        self.db = db
        self.spApi = SpotifyAPI()

    def login(self):
        self.spApi.login()

    def createPlaylist(self, name):
        url, spotify_id, snapshot = self.spApi.createPlaylist(name)
        return url, spotify_id, snapshot

    def addTrack(self, track_url, group_id, user_id):
        track_id = self.db.createTrack(group_id, user_id, track_url, 1)
        return track_id, group_id

    def addTracks(self, tracks, group_id, user_id):
        assert type(tracks) == list
        tracks_id = [self.addTrack(track, group_id, user_id) for track in tracks]
        return tracks_id, group_id

    def removeTrack(self, track_url, group_id, user_id):
        track_id = self.db.createTrack(group_id, user_id, track_url, 0)
        return track_id, group_id

    def removeTracks(self, tracks, group_id, user_id):
        assert type(tracks) == list
        tracks_id = [self.remoceTrack(track, group_id, user_id) for track in tracks]
        return tracks_id, group_id

    def vote(self, track_id, vote):
        if vote == 1:
            vote_value = self.db.getTrack("votes_for", id=track_id)[0][0] + 1
            self.db.updateTrack(track_id, votes_for=vote_value)
        elif vote == 0:
            vote_value = self.db.getTrack("votes_against", id=track_id)[0][0] + 1
            self.db.updateTrack(track_id, votes_against=vote_value)
        else:
            print("Raise some exception or something")
        self.trackUpdate()

    def trackUpdate(self):
        all_tracks = self.db.getTrack()

        for track in all_tracks:
            #Maybe have timedelta as a parameter
            if track[8]:
                continue
            elif datetime.fromisoformat(track[7]) < datetime.now() - timedelta(days=7):
                self.db.updateTrack(track[0], dead=True)
            else:
                group_threshold, playlist_id = self.db.getGroup("group_threshold", "group_playlist_id", id=track[1])[0]
                if track[4] - track[5] > group_threshold:
                    if track[6]:
                        self._addTrackSpotify(track[3], playlist_id)
                    else:
                        self._removeTrackSpotify(track[3], playlist_id)
                    self.db.updateTrack(track[0], dead=True)
            
    def _addTrackSpotify(self, track, playlist_id):
        r = self.spApi.addTracks(playlist_id, [track])
        print(r)

    def _addTracksSpotify(self, tracks, playlist_id):
        assert type(tracks) == list
        r = self.spApi.addTracks(playlist_id, tracks)
        print(r)

    def _removeTrackSpotify(self, track):
        r = self.spApi.removeTracks(playlist_id, [track])
        print(r)

    def _removeTracksSpotify(self, tracks):
        assert type(tracks) == list
        r = self.spApi.removeTracks(playlist_id, tracks)
        print(r)
