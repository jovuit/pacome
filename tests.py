import os
from dotenv import load_dotenv

from spotify import SpotifyAPI
from database import PacomeDB

# Spotify tests
load_dotenv()
testSpotifyAPI = False
testPacomeDB = True

#Spotify API
if testSpotifyAPI:
    spApi = SpotifyAPI()

    spApi.login()
    playlist_url, playlist_id, snapshot = spApi.createPlaylist(os.getenv("TEST_PLAYLIST"))
    spApi.addTracks(playlist_id, ["https://open.spotify.com/track/4N2Z0No2oTM1waCclhfOVL?si=A0dgDO0dSs6LOTBWz9GoFQ", "spotify:track:1npqP0Epwvvc1jzudkkmVB", "https://open.spotify.com/album/7bMpvjE9r9KDv2prNYaVpg?si=0yfZhkShRqyXVc_JVHwqLQ"])
    spApi.removeTracks(playlist_id, ["https://open.spotify.com/track/4N2Z0No2oTM1waCclhfOVL?si=A0dgDO0dSs6LOTBWz9GoFQ", "spotify:track:1npqP0Epwvvc1jzudkkmVB"])
    spApi.deletePlaylist(playlist_id)

#PacomeDB
if testPacomeDB:
    db = PacomeDB(os.getenv("TEST_DB_NAME"))

    assert len(db.getGroupId()) == int(os.getenv("ID_SIZE"))

    playlist_url = "playlist_url"
    playlist_id = "playlist_id"
    a_tgid = 'aaaaaa'
    b_tgid = 'bbbbbb'
    c_tgid = 'cccccc'
    a = db.registerUser(a_tgid)
    b = db.registerUser(b_tgid)
    a_id = db.getUserId(a_tgid)
    b_id = db.getUserId(b_tgid)
    assert a_id == 1
    assert b_id == 2
    assert db.getUserLang(a_tgid) == 0
    assert db.getUserLang(b_tgid) == 0
    #tg_id ou id ?
    group_name1, group_id, playlist_url1 = db.createGroup(os.getenv("TEST_PLAYLIST"), a_tgid, playlist_url, playlist_id)
    assert db.join(group_id, a_tgid) == (group_name1, playlist_url1)
    assert group_name1 == os.getenv("TEST_PLAYLIST")
    assert playlist_url1 == playlist_url
    assert len(group_id) == int(os.getenv("ID_SIZE"))
    group_name2, playlist_url2 = db.join(group_id, b_tgid)
    assert group_name1 == group_name2
    assert playlist_url1 == playlist_url2
    playlist_id1 = db.getUserActivePlaylist(a_tgid)
    playlist_id2 = db.getUserActivePlaylist(b_tgid)
    assert playlist_id1 == playlist_id2
    assert playlist_id1 == playlist_id

    db.updateUser(b_id, user_lang=1, user_tgId=c_tgid)
    assert db.getUserId(c_tgid) == b_id
    assert db.getUserLang(c_tgid) == 1
    assert db.getUserActivePlaylist(c_tgid) == playlist_id1

    assert db.getGroup('id', group_name=os.getenv("TEST_PLAYLIST"))[0][0] == 1
    assert db.getUser('user_active_group', 'id') == [(1, 1), (1,  2)]
    assert db.getUser('user_active_group', user_tgId=a_tgid) == [(1, )]

    all_groups = db.getUserGroups(a_id)
    print(all_groups)

    assert len(db.getAllUsers()) == 2

    db.delete()

print("Everythig ran fine !")
